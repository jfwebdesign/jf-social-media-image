<?php
/**
 * Plugin Name: JF WebDesign Force Featured Image Share
 * Plugin URI: https://www.jfwebdesign.com
 * Description: This is a drop in plugin to force social media to use featured image. Works with Jetpack too. If header image is .png, plugin will default to Custom Logo.
 * Version: 2.5.1
 * Author: JF WebDesign
 * Author URI: https://www.jfwebdesign.com
 */

// remove jetpack open graph tags
remove_action('wp_head','jetpack_og_tags');

//Disable Jetpack's OG tags //
add_filter( 'jetpack_enable_open_graph', '__return_false' );

function add_og_meta_tags() {
    global $post;
	?>
<meta name="title" property="og:title" content="<?php bloginfo( 'name' ); ?>" />

<!--<meta property="og:image:type" content="image/jpeg" />-->
<meta property="og:url" content="<?php bloginfo( 'wpurl' ); ?>" />

<?php 
$page_id = get_queried_object_id();
if ( has_post_thumbnail( $page_id ) ) {
	$id = get_post_thumbnail_id( $page_id );
    $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $page_id ), 'full', false );
    $image = $image_array[0];
	
	// .png check
	if (strpos($image, '.jpg') !== false) {
		$metadata = wp_prepare_attachment_for_js( $id );
		$description = $metadata['description'];
		echo '<meta name="description" property="og:description" content="'.$description.'" />';
		//echo '<meta property="og:image:width" content="'.$image_array[1].'" />';
		//echo '<meta property="og:image:height" content="'.$image_array[2].'" />';
		echo '<meta property="og:image:secure_url" content="'.$image.'?v='.rand().'" />';
		echo '<meta property="og:image" content="'.$image.'?v='.rand().'" />';
	}
	else
	{
			//This will display the custom logo if header image is a .png.
			$custom_logo_id = get_theme_mod( 'custom_logo' );
			$image_array = wp_get_attachment_image_src( $custom_logo_id , 'full', false );
			if ( has_custom_logo() ) {
				
				$metadata = wp_prepare_attachment_for_js( $custom_logo_id );
				$description = $metadata['description'];
				echo '<meta name="description" property="og:description" content="'.$description.'" />';
				$image = esc_url( $image_array[0] );
			//echo '<meta property="og:image:width" content="'.$image_array[1].'" />';
			//echo '<meta property="og:image:height" content="'.$image_array[2].'" />';
			echo '<meta property="og:image:secure_url" content="'.$image.'?v='.rand().'" />';
			echo '<meta property="og:image" content="'.$image.'?v='.rand().'" />';
			} 
			else {

			}
		
	}
	//End .png check
	
}
else {
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$image_array = wp_get_attachment_image_src( $custom_logo_id , 'full', false );
		if ( has_custom_logo() ) {
			$metadata = wp_prepare_attachment_for_js( $custom_logo_id );
			$description = $metadata['description'];
			echo '<meta name="description" property="og:description" content="'.$description.'" />';
			$image = esc_url( $image_array[0] );
			//echo '<meta property="og:image:width" content="'.$image_array[1].'" />';
			//echo '<meta property="og:image:height" content="'.$image_array[2].'" />';
			echo '<meta property="og:image:secure_url" content="'.$image.'?v='.rand().'" />';
			echo '<meta property="og:image" content="'.$image.'?v='.rand().'" />';
		} 
	else {
			}
	}
}
 add_action( 'wp_head', 'add_og_meta_tags', 1 , 2 );

?>